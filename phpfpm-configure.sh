#!/bin/bash

if [ "$PHP_INI" ]; then
    mv /usr/local/etc/php/config/php-$PHP_INI.ini /usr/local/etc/php/php.ini
fi

if [ "$PHPFPM_POOL" ]; then
    mv /usr/local/etc/php-fpm/config/www.conf.$PHPFPM_POOL /usr/local/etc/php-fpm.d/www.conf
fi

if [ "$ENABLE_OPCACHE" = true ]; then
    sed -i "s/opcache.enable=0/opcache.enable=1/" "/usr/local/etc/php/conf.d/opcache.ini"
fi

if [ "$ENABLE_XDEBUG" = true ]; then
    mv /usr/local/etc/php/config/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
fi

if [ "$XDEBUG_IDEKEY" ]; then
    sed -i "s/;xdebug.idekey=/xdebug.idekey=$XDEBUG_IDEKEY/" "/usr/local/etc/php/conf.d/xdebug.ini"
fi

if [ "$XDEBUG_REMOTEHOST" ]; then
    sed -i "s/xdebug.remote_host=localhost/xdebug.remote_host=$XDEBUG_REMOTEHOST/" "/usr/local/etc/php/conf.d/xdebug.ini"
fi

if [ "$NEWRELIC_LICENSE" ]; then
   sed -i "s/newrelic.license = \"REPLACE_WITH_REAL_KEY\"/newrelic.license = \"$NEWRELIC_LICENSE\"/" "/usr/local/etc/php/conf.d/newrelic.ini"
fi

if [ "$NEWRELIC_APPNAME" ]; then
   sed -i "s/newrelic.appname = \"PHP Application\"/newrelic.appname = \"$NEWRELIC_APPNAME\"/" "/usr/local/etc/php/conf.d/newrelic.ini"
fi

if [ "$NEWRELIC_ENABLED" ]; then
    sed -i "s/;newrelic.enabled = true/newrelic.enabled = $NEWRELIC_ENABLED/" "/usr/local/etc/php/conf.d/newrelic.ini"
fi

sed -i "s/;newrelic.framework = \"\"/newrelic.framework = \"zend\"/" "/usr/local/etc/php/conf.d/newrelic.ini"