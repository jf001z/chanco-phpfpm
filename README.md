# Chanco Docker PHP-FPM #
This image should be used as a common base for all Chanco applications that require php-fpm. It takes care of installation of all of our common components, and provides a single point for upgrading PHP as well as other common modules/extensions.

## What does this image do? ##

* Sets the correct timezone
* Installs Composer
* Installs Services:
    - libldap2-dev
    - zlib1g-dev
    - libicu-dev
    - g++
    - vim
    - libmcrypt-dev
    - php-pear
    - curl
    - wget
    - git
    - zip
    - supervisor
    - cron
    - sendmail-bin
    - libxml2-dev
* Installs PHP Extensions:
    - ldap
    - pdo_mysql
    - mcrypt
    - intl
    - zip
    - xml

* Installs Node and Bower
* Installs Xdebug
* Installs New Relic APM
* Installs Supervisord
* Copies our deployment keys
* Runs PHP-FPM through supervisord

## Custom Configurations ##
Applications may wish to install further services and extensions which can be done in their own Dockerfile.

## How do I add a cron/worker? ##
If your application needs workers create a new file in your application:

php/config/supervisord/conf.d/exampleWorker.php
```
    [program:ReviewsWorker]
    command=/usr/local/bin/php console worker:process-reviews
    process_name=%(program_name)s_%(process_num)02d
    stdout_logfile=/var/log/supervisor/%(program_name)s.log
    numprocs=1
    directory=/var/www/html
    autostart=true
    autorestart=true
    user = www-data
```

In your PHP service Dockerfile, ensure that file is copied into the correct location (/etc/supervisor/conf.d/) within the container. Supervisord will read all *.conf files in that directory

Dockerfile
```
    ........
    COPY php/config/supervisord/conf.d /etc/supervisor/conf.d/
    ........
```

## I want to build my own image using docker-phpfpm as a base
If you want to use this image as a base for yours that's all fine!

Command: `FROM worldstores/phpfpm:<tag>`

Just remember that if you override our run.sh script with a version of your own you should run
`/usr/local/bin/phpfpm-configure.sh` within your `run.sh` script 

