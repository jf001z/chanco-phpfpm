FROM php:7.1.10-fpm

# -----------------------------------------------------------------------------
# SET CORRECT TIMEZONE
# -----------------------------------------------------------------------------
RUN mv /etc/localtime /etc/localtime.bak \
    && ln -s /usr/share/zoneinfo/Europe/London /etc/localtime

# -----------------------------------------------------------------------------
# COPY SUPERVISOR CONF
# -----------------------------------------------------------------------------
COPY config/supervisord/supervisord.conf /etc/
COPY config/supervisord/conf.d /etc/supervisor/conf.d/

# -----------------------------------------------------------------------------
# PHP ini and pool configurations
# -----------------------------------------------------------------------------
COPY config/php /usr/local/etc/php/config
COPY config/pool /usr/local/etc/php-fpm/config

# -----------------------------------------------------------------------------
# COMPOSER INSTALLATION
# -----------------------------------------------------------------------------
RUN apt-get update \
&&  apt-get install curl git unzip -y
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# -----------------------------------------------------------------------------
# NODEJS REPOSITORY
# -----------------------------------------------------------------------------

RUN  apt-get install gnupg2 -y \
&&  curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install nodejs -y
# -----------------------------------------------------------------------------
# YARN REPOSITORY
# -----------------------------------------------------------------------------
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN  apt-get update \
&&  apt-get install yarn -y

# -----------------------------------------------------------------------------
# SERVICES & PHP EXTENSIONS
# -----------------------------------------------------------------------------
RUN \
    apt-get update && \
    apt-get install -y \
        libldap2-dev \
        zlib1g-dev \
        libicu-dev \
        g++ \
        vim \
        libmcrypt-dev \
        curl \
        wget \
        git \
        zip \
        supervisor \
        cron \
        sendmail-bin \
        libxml2-dev \
    && npm install -g bower \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-install ldap \
    && docker-php-ext-install pcntl \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install mysqli \
    && docker-php-ext-install mcrypt \
    && docker-php-ext-install intl \
    && docker-php-ext-install opcache \
    && docker-php-ext-install zip \
    && docker-php-ext-install xml

# -----------------------------------------------------------------------------
# OPCACHE INSTALLATION
# -----------------------------------------------------------------------------
RUN mv /usr/local/etc/php/config/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

RUN yes | pecl install xdebug \
    && echo >> /usr/local/etc/php/config/xdebug.ini \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" >> /usr/local/etc/php/config/xdebug.ini

RUN touch /var/log/xdebug.log && chmod 666 /var/log/xdebug.log

# -----------------------------------------------------------------------------
# NEWRELIC INSTALLATION
# -----------------------------------------------------------------------------
#RUN mkdir /usr/local/lib/newrelic/ \
#    && cd /usr/local/lib/newrelic/ \
#    && wget -r -nd --no-parent -Alinux.tar.gz http://download.newrelic.com/php_agent/release/ >/dev/null 2>&1 \
#    && export NR_INSTALL_SILENT=true \
#    && mv newrelic-php5-* newrelic-php5-linux.tar.gz \
#    && tar -xzvf newrelic-php5-linux.tar.gz --strip=1 \
#    && ./newrelic-install install

# -----------------------------------------------------------------------------
# DEFAULT ENVIRONMENT VARIABLES
# -----------------------------------------------------------------------------
ENV APP_ENV dev
ENV BOWER_INSTALL false
ENV COMPOSER_INSTALL false
ENV DB_INIT false
ENV ENABLE_XDEBUG false
ENV ENABLE_OPCACHE false
ENV PHP_INI dev
ENV PHPFPM_POOL dev

# -----------------------------------------------------------------------------
# Copy Run entry script
# -----------------------------------------------------------------------------
COPY /phpfpm-run.sh /usr/local/bin/

# -----------------------------------------------------------------------------
# Copy Configuration script
# -----------------------------------------------------------------------------
COPY /phpfpm-configure.sh /usr/local/bin/

# -----------------------------------------------------------------------------
# Copy Deployment Keys for Private Dependency Installation
# -----------------------------------------------------------------------------
COPY .ssh/webdeploy /root/.ssh

# -----------------------------------------------------------------------------
# Set correct permissions
# -----------------------------------------------------------------------------
RUN chmod 755 /usr/local/bin/phpfpm-run.sh \
    && chmod 755 /usr/local/bin/phpfpm-configure.sh \
    && chmod -R 600 /root/.ssh/

CMD ["/usr/local/bin/phpfpm-run.sh"]