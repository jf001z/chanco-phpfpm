#!/bin/bash

# Run configuration script first
/usr/local/bin/phpfpm-configure.sh

# Start Supervisor and services
exec /usr/bin/supervisord -c /etc/supervisord.conf
